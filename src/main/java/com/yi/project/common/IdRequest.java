package com.yi.project.common;

import lombok.Data;

import java.io.Serializable;

/**
 * 根据Id操作请求
 *
 * @author yi
 */
@Data
public class IdRequest implements Serializable {
    /**
     * id
     */
    private Long id;

    private static final long serialVersionUID = 1L;
}