package com.yi.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yi.project.model.dto.interfaceinfo.InterfaceInfoInvokeRequest;
import com.yi.yiapicommon.model.entity.InterfaceInfo;

import javax.servlet.http.HttpServletRequest;


/**
* @author YOGA
* @description 针对表【interface_info(接口信息)】的数据库操作Service
* @createDate 2023-05-20 16:17:56
*/
public interface InterfaceInfoService extends IService<InterfaceInfo> {
    /**
     * 校验
     *
     * @param interfaceInfo
     * @param add 是否为创建校验
     */
    void validInterfaceInfo(InterfaceInfo interfaceInfo, boolean add);

    /**
     * 用户调用接口
     * @param interfaceInfoInvokeRequest
     * @param request
     * @return
     */
    String invokeInterfaceInfo(InterfaceInfoInvokeRequest interfaceInfoInvokeRequest, HttpServletRequest request);
}
