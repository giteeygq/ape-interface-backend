package com.yi.project.service.impl.inner;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yi.project.common.ErrorCode;
import com.yi.project.exception.BusinessException;
import com.yi.project.mapper.UserMapper;
import com.yi.yiapicommon.model.entity.User;
import com.yi.yiapicommon.service.InnerUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;

import javax.annotation.Resource;

//每次调用接口都是实时获得当前用户的ak，sk传入到client-sdk,然后sdk通过请求头将ak，sign(sk)发送给gateway
//然后gateway会根据当前收到的请求头ak，从数据库获得相应sk并使用相同的签名算法来鉴权
@DubboService
public class InnerUserServiceImpl implements InnerUserService {

    @Resource
    UserMapper userMapper;

    @Override
    public User getInvokeUser(String accessKey) {
        if(StringUtils.isAnyBlank(accessKey)){
            throw new BusinessException(ErrorCode.NOT_FOUND_ERROR);
        }
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("accessKey", accessKey);
        User user = userMapper.selectOne(userQueryWrapper);
        return user;
    }
}
