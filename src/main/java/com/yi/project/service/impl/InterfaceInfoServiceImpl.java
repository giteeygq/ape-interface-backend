package com.yi.project.service.impl;

import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yi.apiclientsdk.client.YiApiClient;
import com.yi.project.common.ErrorCode;
import com.yi.project.exception.BusinessException;
import com.yi.project.model.dto.interfaceinfo.InterfaceInfoInvokeRequest;
import com.yi.project.model.enums.InterfaceInfoStatusEnum;
import com.yi.project.service.InterfaceInfoService;
import com.yi.project.mapper.InterfaceInfoMapper;
import com.yi.project.service.UserService;
import com.yi.yiapicommon.model.entity.InterfaceInfo;
import com.yi.yiapicommon.model.entity.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
* @author YOGA
* @description 针对表【interface_info(接口信息)】的数据库操作Service实现
* @createDate 2023-05-20 16:17:56
*/
@Service
public class InterfaceInfoServiceImpl extends ServiceImpl<InterfaceInfoMapper, InterfaceInfo>
    implements InterfaceInfoService {

    @Resource
    UserService userService;

    @Override
    public void validInterfaceInfo(InterfaceInfo interfaceInfo, boolean add) {

    }

    @Override
    public String invokeInterfaceInfo(InterfaceInfoInvokeRequest interfaceInfoInvokeRequest, HttpServletRequest request) {
        if (interfaceInfoInvokeRequest == null || interfaceInfoInvokeRequest.getId() <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        long id = interfaceInfoInvokeRequest.getId();
        String userRequestParams = interfaceInfoInvokeRequest.getUserRequestParams();
        // 判断是否存在
        InterfaceInfo oldInterfaceInfo = this.getById(id);
        if (oldInterfaceInfo == null) {
            throw new BusinessException(ErrorCode.NOT_FOUND_ERROR);
        }
        //判断接口是否下线
        if(oldInterfaceInfo.getStatus() == InterfaceInfoStatusEnum.OFFLINE.getValue()){
            throw new BusinessException(ErrorCode.NO_AUTH_ERROR,"接口下线，请联系管理员");
        }
        //当前只是使用了一个固定接口，后续需要根据id调用不同的模拟接口
        User loginUser = userService.getLoginUser(request);
        String accessKey = loginUser.getAccessKey();
        String secretKey = loginUser.getSecretKey();
        YiApiClient yiApiClient = new YiApiClient(accessKey, secretKey);
        //得到接口方法进行调用
        String interfaceInfoName = oldInterfaceInfo.getName();
        String result = ReflectUtil.invoke(yiApiClient, interfaceInfoName, userRequestParams == null ? "" : userRequestParams);
        return result;
    }
}




