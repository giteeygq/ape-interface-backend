package com.yi.project.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yi.yiapicommon.model.entity.UserInterfaceInfo;

/**
* @author YOGA
* @description 针对表【user_interface_info(用户调用接口关系)】的数据库操作Service
* @createDate 2023-05-24 20:33:19
*/
public interface UserInterfaceInfoService extends IService<UserInterfaceInfo> {

    void validUserInterfaceInfo(UserInterfaceInfo userInterfaceInfo, boolean b);

    /**
     * 调用接口统计
     * @param interfaceInfoId
     * @param userId
     * @return
     */
    boolean invokeCount(long interfaceInfoId, long userId);
}
