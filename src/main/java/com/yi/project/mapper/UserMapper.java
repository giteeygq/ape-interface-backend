package com.yi.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yi.yiapicommon.model.entity.User;


/**
 * @Entity com.yi.project.model.domain.User
 */
public interface UserMapper extends BaseMapper<User> {

}




