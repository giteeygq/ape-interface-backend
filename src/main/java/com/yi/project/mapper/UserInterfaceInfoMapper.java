package com.yi.project.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yi.yiapicommon.model.entity.UserInterfaceInfo;

import java.util.List;

/**
* @author YOGA
* @description 针对表【user_interface_info(用户调用接口关系)】的数据库操作Mapper
* @createDate 2023-05-24 20:33:19
* @Entity com.yi.project.model.entity.UserInterfaceInfo
*/
public interface UserInterfaceInfoMapper extends BaseMapper<UserInterfaceInfo> {

    List<UserInterfaceInfo> listTopInvokeInterfaceInfo(int limit);
}




