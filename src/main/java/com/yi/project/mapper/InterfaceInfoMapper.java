package com.yi.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yi.yiapicommon.model.entity.InterfaceInfo;

/**
* @author YOGA
* @description 针对表【interface_info(接口信息)】的数据库操作Mapper
* @createDate 2023-05-20 16:17:56
* @Entity com.yi.project.model.entity.InterfaceInfo
*/
public interface InterfaceInfoMapper extends BaseMapper<InterfaceInfo> {

}




