package com.yi.project.service.impl.inner;

import com.yi.project.mapper.InterfaceInfoMapper;
import com.yi.yiapicommon.model.entity.InterfaceInfo;
import com.yi.yiapicommon.service.InnerInterfaceInfoService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class InnerInterfaceInfoServiceImplTest {

    @Resource
    private InterfaceInfoMapper interfaceInfoMapper;

    @DubboReference
    InnerInterfaceInfoService innerInterfaceInfoService;

    @Test
    void getInterfaceInfo() {
        String url = "http://localhost:8123/api/name/user";
        String method = "POST";
        InterfaceInfo interfaceInfo = innerInterfaceInfoService.getInterfaceInfo(url, method);
        System.out.println(interfaceInfo);
    }
}