package com.yi.yiapiinterface;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.yi.apiclientsdk.client.YiApiClient;
import com.yi.apiclientsdk.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import javax.xml.transform.TransformerConfigurationException;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import static com.yi.apiclientsdk.utils.SignUtils.genSign;

@SpringBootTest
class YiapiInterfaceApplicationTests {

    @Test
    void testAPI(){
        Map<String,String> map= new HashMap<>();

        HttpResponse httpResponse = HttpRequest.get("https://api.vvhan.com/api/view?type=json")
                .addHeaders(map)
                .execute();

        System.out.println(httpResponse.body());

    }

    @Test
    void testAPI2(){
        HttpResponse httpResponse = HttpRequest.get("https://api.btstu.cn/yan/api.php?charset=utf-8&encode=json").execute();
        String result = httpResponse.body();
        System.out.println(result);
    }



}
