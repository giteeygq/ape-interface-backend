package com.yi.yiapiinterface.controller;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/botian")
public class BotianController {

    @GetMapping("/translate")
    public String getTranslatedTextByGet(String text){
        HttpResponse httpResponse = HttpRequest.get("http://api.btstu.cn/tst/api.php?text=" + text).execute();
        String result = httpResponse.body();
        return result;
    }

    @GetMapping("/chicken")
    public String getPoisonousChickenSoup(){
        HttpResponse httpResponse = HttpRequest.get("https://api.btstu.cn/yan/api.php?charset=utf-8&encode=json").execute();
        String result = httpResponse.body();
        return result;
    }

    @GetMapping("/sougou")
    public String getSougouInclusionVolume(String domain){
        HttpResponse httpResponse = HttpRequest.get("https://api.btstu.cn/sgics/api.php?domain=" + domain).execute();
        String result = httpResponse.body();
        return result;
    }

    @GetMapping("/doutu")
    public String getDoutu(){
        HttpResponse httpResponse = HttpRequest.get("https://api.vvhan.com/api/view?type=json").execute();
        String result = httpResponse.body();
        return result;
    }



}
