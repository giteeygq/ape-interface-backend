package com.yi.yiapiinterface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YiapiInterfaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(YiapiInterfaceApplication.class, args);
    }

}
