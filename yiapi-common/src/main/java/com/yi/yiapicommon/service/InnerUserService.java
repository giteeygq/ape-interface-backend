package com.yi.yiapicommon.service;

import com.yi.yiapicommon.model.entity.User;



/**
 * 用户服务
 *
 * @author yi
 */
public interface InnerUserService {

    User getInvokeUser(String accessKey);


}
