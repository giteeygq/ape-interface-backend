package com.yi.yiapicommon.service;

import com.yi.yiapicommon.model.entity.InterfaceInfo;


/**
* @author YOGA
* @description 针对表【interface_info(接口信息)】的数据库操作Service
* @createDate 2023-05-20 16:17:56
*/
public interface InnerInterfaceInfoService {

    InterfaceInfo getInterfaceInfo(String url, String method);
}
