package com.yi.apiclientsdk;

import com.yi.apiclientsdk.client.YiApiClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("yi.client")
@ComponentScan
public class YiApiClientConfig {

    private String accessKey;

    private String secretKey;

    @Bean
    public YiApiClient yiApiClient(){
        return new YiApiClient(accessKey, secretKey);
    }
}
